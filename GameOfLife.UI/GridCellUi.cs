﻿using GameOfLife.Engine.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife.UI
{
    public class GridCellUi : GridCell
    {
        public readonly Point Location;

        public GridCellUi(int x, int y, Point location, CellState state = CellState.Dead) : base(x, y, state)
        {
            Location = location;
        }
    }
}
