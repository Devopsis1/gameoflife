﻿namespace GameOfLife.Engine.Models
{
    /// <summary>
    /// Représente une cellule localisée et dans un état défini (Vivante ou Morte)
    /// </summary>
    public class GridCell
    {
        public readonly int X;
        public readonly int Y;
        public CellState State { get; private set; }

        public GridCell(int x, int y, CellState state = CellState.Dead)
        {
            X = x;
            Y = y;
            State = state;
        }

        public void ChangeState(CellState newState)
        {
            State = newState;
        }

        public override string ToString()
        {
            return string.Concat("Cellule [", X, ";", Y, "] - ", State);
        }
    }

    public enum CellState
    {
        Alive,
        Dead
    }
}